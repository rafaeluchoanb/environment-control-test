package enums;

public enum EnvEnum {
    UAT("uat"),
    DEV("dev"),
    PRD("prd");

    private final String name;

    EnvEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
