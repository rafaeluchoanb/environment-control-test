package configs;


import org.aeonbits.owner.Config;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "system:env",
        "classpath:${env}.properties"
})
public interface EnvironmentConfig extends Config {
        @Key("url")
        String url();
}
