package bases;


import configs.Environment;
import configs.EnvironmentConfig;
import enums.EnvEnum;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;
import java.util.Optional;

import static java.util.Objects.isNull;

public class BaseTest {
    protected EnvironmentConfig config;
    private static final String MAIN_ENV = System.getProperty("env");
    private static final String DEFAULT_ENV = EnvEnum.UAT.getName();

    @BeforeMethod(alwaysRun = true)
    public void setup(Method method) {
        String environment = Optional.ofNullable(MAIN_ENV)
                .orElseGet(() -> Optional.ofNullable(getEnvironment(method))
                        .orElse(DEFAULT_ENV));
        configureEnvironment(environment);
    }


    private String getEnvironment(Method method) {
        Environment environmentAnnotation = method.getAnnotation(Environment.class);
        return environmentAnnotation != null ? environmentAnnotation.value().getName() : null;
    }

    private void configureEnvironment(String env) {
        System.setProperty("env", env);
        config = ConfigFactory.create(EnvironmentConfig.class);
    }
}
