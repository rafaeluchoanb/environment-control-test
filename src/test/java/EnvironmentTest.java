import bases.BaseTest;
import configs.Environment;
import org.testng.annotations.Test;

import static enums.EnvEnum.*;

public class EnvironmentTest extends BaseTest {

    @Environment(DEV)
    @Test
    public void testEnvironmentDev() {
        System.out.println("O ambiente atual é: " + System.getProperty("env"));
        System.out.println("URL: " + config.url());
    }

    @Environment(UAT)
    @Test
    public void testEnvironmentUat() {
        System.out.println("O ambiente atual é: " + System.getProperty("env"));
        System.out.println("URL: " + config.url());
    }

    @Environment(PRD)
    @Test
    public void testEnvironmentPrd() {
        System.out.println("O ambiente atual é: " + System.getProperty("env"));
        System.out.println("URL: " + config.url());
    }
}
